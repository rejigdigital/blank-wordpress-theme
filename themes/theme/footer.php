<?php
/*
Theme Name: New Theme
Theme URI: http://www.rejigdigital.com
Description: 
Version: 
Author: David R Bova & Iain Gorman
Author URI: http://www.rejigdigital.com
*/
?>

</div>

<footer id="footer">
</footer>

<?php
	wp_footer();
?>

<!-- GOOGLE ANALYTICS START -->
<script type="text/javascript">

</script>
<!-- GOOGLE ANALYTICS END -->

<!-- CAROUSEL -->
<script src="<?php bloginfo('template_url'); ?>/js/easing.js" type="text/javascript" ></script>
<script src="<?php bloginfo('template_url'); ?>/js/caro.js" type="text/javascript"></script>
<!-- CAROUSEL END -->

<!-- FANCYBOX -->
<script src="<?php bloginfo('template_url') ;?>/js/jquery.fancybox.pack.js" type="text/javascript" ></script>
<script src="<?php bloginfo('template_url') ;?>/js/expander.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url') ;?>/js/custom-expander.js" type="text/javascript"></script>
<!-- FANCYBOX END-->

<!-- VALIDATION -->
<script src="<?php bloginfo('template_url') ;?>/js/jquery.validationEngine-en.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url') ;?>/js/jquery.validationEngine.js" type="text/javascript"></script>
<!-- VALIDATION END -->

<!-- SHARE THIS -->

<!-- SHARE THIS END -->

<!-- GENERAL -->
<script src="<?php bloginfo('template_url'); ?>/js/common.js" type="text/javascript"></script>
<!-- GENERAL END -->

</body>
</html>