<?php

/*
Theme Name: New Theme
Theme URI: http://www.rejigdigital.com
Description: 
Version: 
Author: David R Bova & Iain Gorman
Author URI: http://www.rejigdigital.com
*/

get_header(); ?>

				<h1><?php
					printf( __( 'Category Archives: %s', 'twentyten' ), '' . single_cat_title( '', false ) . '' );
				?></h1>
				<?php
					$category_description = category_description();
					if ( ! empty( $category_description ) )
						echo '' . $category_description . '';

				/* Run the loop for the category page to output the posts.
				 * If you want to overload this in a child theme then include a file
				 * called loop-category.php and that will be used instead.
				 */
				get_template_part( 'loop', 'category' );
				?>

<?php get_sidebar(); ?>
<?php get_footer(); ?>