<?php
/*
Theme Name: New Theme
Theme URI: http://www.rejigdigital.com
Description: 
Version: 
Author: David R Bova & Iain Gorman
Author URI: http://www.rejigdigital.com
*/
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" > 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 

<!-- DELETE WHEN LIVE -->
<meta name="robots" content="noindex, nofollow">

<title><?php	wp_title();	?></title>
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	//wp_enqueue_script( 'comment-reply' );
	wp_head();
?>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>


<link rel="stylesheet" href="<?php bloginfo('template_url') ;?>/css/validationEngine.jquery.css" type="text/css"/>
<link rel="stylesheet" href="<?php bloginfo('template_url') ;?>/js/jquery.fancybox.css" type="text/css" media="screen" />
<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.ico" />
<!-- BROWSER SPECIFIC CONDITIONALS START -->	
<!--[If IE 7]>
	<link type="text/css" media="screen" rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/ie7.css" />
<![endif]-->
<!--[If IE 8]>
<![endif]-->
<!--[if lte IE 6]>
<link rel="stylesheet" href="http://universal-ie6-css.googlecode.com/files/ie6.1.1.css" media="screen, projection">
<![endif]-->
<!-- BROWSER SPECIFIC CONDITIONALS END -->	
<script>if (location.hostname == "localhost") { document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=2"></' + 'script>') }</script>
</head>
<body>